﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExitApplications
{
    class Apps
    {
        public string Name { get; set; }
        DbServiceSQL DbsSql = new DbServiceSQL();
        
        public Apps()
        {

        }

        /// <summary>
        /// מייבא שמות אפליקציות
        /// </summary>
        public DataTable ImportAppsNames()
        {
            DataTable DataNamesTable = new DataTable();
            string Qry = $@"SELECT Apps,IconPath
                            FROM AppsForUpdate";
            DataNamesTable= DbsSql.executeSelectQueryNoParam(Qry);
            return DataNamesTable;

        }

        public void ReleaseVersion(string AppName)
        {
            string qry = $@"UPDATE AppsForUpdate
                          SET Change=1
                          WHERE Apps='{AppName}";
            DbsSql.executeSelectQueryForDelete(qry);
            //צריך להחזיר חזרה ל0 ,לעשות דרך סקדואל טסק?
        }
    }
}
