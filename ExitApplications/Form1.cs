﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ExitApplications
{
    public partial class Form1 : Form
    {
        Apps AppsList = new Apps();
        DataTable DataNamesTable;
        public Form1()
        {
            InitializeComponent();
            DataNamesTable= AppsList.ImportAppsNames();//ייבוא שמות אפליקציות
            CreateButtons();


        }

        /// <summary>
        /// יצירת כפתור לכל אפליקציה
        /// </summary>
        private void CreateButtons()
        {
            Button button; 
            int y = 202;
            int x = 922;
            int height = 77;
            int width = 153;
            for (int i = 0; i < DataNamesTable.Rows.Count; i++)
            {
                button = new Button();
                button.Text = DataNamesTable.Rows[i]["Apps"].ToString();
                button.Font = new Font("arial", 12F, FontStyle.Bold);
                button.AutoSize = true;
                button.Location = new Point(x, y);
                button.Size = new Size(width, height);
                button.BackColor = Color.LightGreen;
                if(!string.IsNullOrEmpty( DataNamesTable.Rows[i]["IconPath"].ToString()))
                button.Image = new Icon(DataNamesTable.Rows[i]["IconPath"].ToString(), new Size(48, 48)).ToBitmap();
                //button.Image = new Icon(FileName(), new Size(48, 48)).ToBitmap();
                button.Click += new EventHandler(button_Click);
                x = x - 271;
                if(i%4==0 && i>0)//כל 4 ירד שורה
                {
                    x = 922;
                    y += 128;
                    button.Location = new Point(x, y);
                }
                this.Controls.Add(button);
                
            }
            
        }

        /// <summary>
        /// לחיצה על כפתור של אחת האפליקציות
        /// </summary>
        private void button_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            DialogResult dialogResult = MessageBox.Show($@"? {button.Text} האם ברצונך לעדכן גרסה עבור", "עדכון גרסה", MessageBoxButtons.YesNo);
            if(dialogResult==DialogResult.Yes)
            {
                AppsList.ReleaseVersion(button.Text);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        public string FileName()
        {
            string FileIcon = "";
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    FileIcon = ofd.FileName;
                    //MessageBox.Show(FileName);
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return FileIcon;
        }
    }
}
